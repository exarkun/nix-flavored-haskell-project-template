# nix-flavored-haskell-project-template

## Description

This is a Git repository that can be cloned and used as a starting point for a new Haskell project.
It includes a Nix flake that supplies Haskell goodness from ``hs-flake-utils``.

## Usage

1. Clone this repository.
2. Customize the metadata in flake.nix.
3. Point the upstream at the home for some new project you want to start.
4. Develop with the help of lots of Nix-y Haskell-y goodness.

## Contributing

If you find a problem or want to suggest a new feature,
open an issue in the issue tracker.

## License

This template is made available under the following licenses:

* BSD (3 or 4 clause)
* Apache License v2
* MIT / X11
* LGPL and GPL (all versions)
